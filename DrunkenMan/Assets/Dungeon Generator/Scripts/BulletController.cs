﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    bool hasEffect;
    bool hasDelay;
    bool onceOff;
    float delayTime;
    [SerializeField] float speed = 5f;
    [SerializeField] float lifeTime;
    [SerializeField] bool causeEffect;
    [SerializeField] string invocableFunction;

    // Start is called before the first frame update
    void Start () {
        StartCoroutine (LifeTimer ());
    }

    IEnumerator LifeTimer () {
        yield return new WaitForSeconds (lifeTime);
        Destroy (gameObject);
    }

    // Update is called once per frame
    void Update () {
        transform.position += transform.up * -1 * Time.deltaTime * speed;

    }

    private void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Enemy") {
            Destroy (gameObject);
        }
    }

    public void AssignEffects (bool _hasEffect, bool _hasDelay, bool _onceOff, float _delay, string _invocableFunction) {
        Debug.Log("Assigning");
        hasEffect = _hasEffect;
        hasDelay = _hasDelay;
        onceOff = _onceOff;
        delayTime = _delay;
        invocableFunction = _invocableFunction;
        Debug.Log("Assigned Attributes");

        if (hasEffect) {
            StartCoroutine (EffectLoop ());
        }
    }

    IEnumerator EffectLoop () {
        Debug.Log("Effect Loop");
        if (onceOff) {
            AttachmentsEffects.instance.InvokeEffect (invocableFunction, gameObject);
        } else {
            causeEffect = true;
        }
        while (causeEffect) {
            AttachmentsEffects.instance.InvokeEffect (invocableFunction, gameObject);
            if (hasDelay) {
                yield return new WaitForSeconds (delayTime);
            } else {
                yield return null;
            }
        }
    }
}