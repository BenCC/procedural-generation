﻿using UnityEngine;

[CreateAssetMenu (fileName = "Attachment", menuName = "Attachment", order = 0)]
public class Attachment : ScriptableObject {
    public string attachmentName;
    public Sprite image;
    public bool hasEffect;
    public bool onBullet;
    public string functionName;
    public bool onBulletEffectOnceOff;
    public bool onBulletEffectHasDelay;
    public float onBulletEffectDelay;
    public bool affectsStats;
    public float statFireRateMultiplier;
    public float statDamageMultiplier;
    public bool negativeFireRate;
    public bool negativeDamage;
    [Range (1, 100)]
    public int spawnPercentage = 1;
    [Range (1, 999)]
    public int levelThreshold = 1;
    public bool stopsDropping;
    [Range (1, 999)]
    public int lastLevelCanSpawn = 1;
}