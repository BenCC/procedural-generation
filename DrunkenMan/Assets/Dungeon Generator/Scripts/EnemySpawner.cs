﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    [SerializeField] GameObject EnemyPrefab;
    public float timeBetweenSpawns;
    public int enemiesPerSpawn;
    [SerializeField] bool spawnEnemies;

    private void Start () {
        spawnEnemies = false;
        StartCoroutine (DelaySpawn ());
    }

    IEnumerator DelaySpawn () {
        yield return new WaitForSeconds (5f);
        spawnEnemies = true;
        StartCoroutine(SpawnTheOpposition());
    }

    IEnumerator SpawnTheOpposition () {
        while (spawnEnemies) {
            //Debug.Log ("spawning");
            Instantiate (EnemyPrefab, this.transform);
            yield return new WaitForSeconds (timeBetweenSpawns);
        }

    }

}