﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class treasure : MonoBehaviour
{
    public Attachment thisAttachment;
    public SpriteRenderer imageToChange;
    public Sprite image;
    GameObject player;

    private void Start() {
        imageToChange.sprite = image;
        //Debug.Log("spawned");
    }
    public void PickUp(){
        player.GetComponent<PlayerController>().AddAttachment(thisAttachment);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player"){
            player = other.gameObject;
        }
    }
}
