using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float speed;

    private Rigidbody2D rb;
    private Camera camera;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] Transform bulletSpawnPoint;
    [SerializeField] bool nearExit;
    bool nearChest = false;
    bool nearTreasure = false;
    GameObject objectNearPlayer;

    Animator anim;

    [Header ("Weapon Stats")]
    [SerializeField] float fireRate;
    [SerializeField] float damage;
    [SerializeField] List<Attachment> attachments;
    [SerializeField] int attachmentSlots;
    [SerializeField] int attachmentsInUse;
    [SerializeField] bool canFire = true;
    bool firing = false;
    void Start () {
        //Cursor.lockState = CursorLockMode.Confined;
        camera = Camera.main;
        rb = GetComponent<Rigidbody2D> ();
    }

    private void Update () {
        if (Input.GetMouseButtonDown (0) && canFire) {
            firing = true;
            StartCoroutine (WeaponFire ());
        }
        if (Input.GetMouseButtonUp (0)) {
            firing = false;
        }
        if (Input.GetKeyDown (KeyCode.E)) {
            if (nearChest) {
                nearChest = false;
                objectNearPlayer.GetComponent<TreasureController> ().Open ();
                objectNearPlayer = null;
            } else if (nearTreasure) {
                nearTreasure = false;
                objectNearPlayer.GetComponent<treasure> ().PickUp ();
                objectNearPlayer = null;
            } else if (nearExit) {
                nearExit = false;
                objectNearPlayer.GetComponent<ExitController> ().ExitLevel ();
            }
        }
    }

    public void AddAttachment (Attachment _attachment) { //checks if available slots, else replaces first one, also updates stats for player
        if (attachmentsInUse == attachmentSlots) {
            if (attachments[0].affectsStats) {
                if (_attachment.negativeFireRate) {
                    fireRate = fireRate / _attachment.statFireRateMultiplier;
                } else {
                    fireRate = fireRate * _attachment.statFireRateMultiplier;
                }

                if (_attachment.negativeDamage) {
                    damage = damage / _attachment.statDamageMultiplier;
                } else {
                    damage = damage * _attachment.statDamageMultiplier;
                }
            }
            attachments.RemoveAt (0);
            attachments.Add (_attachment);
        } else if (attachmentsInUse < attachmentSlots) {
            attachmentsInUse++;
            attachments.Add (_attachment);
        }
        if (_attachment.affectsStats) {
            if (_attachment.negativeFireRate) {
                fireRate = fireRate * _attachment.statFireRateMultiplier;
            } else {
                fireRate = fireRate / _attachment.statFireRateMultiplier;
            }

            if (_attachment.negativeDamage) {
                damage = damage * _attachment.statDamageMultiplier;
            } else {
                damage = damage / _attachment.statDamageMultiplier;
            }
        }
    }

    private void OnTriggerEnter2D (Collider2D other) { //Allows player to interact with objects when close
        bool addObject = false;
        if (other.tag == "treasure") {
            nearTreasure = true;
            addObject = true;
        } else if (other.tag == "chest") {
            nearChest = true;
            addObject = true;
        } else if (other.tag == "exit") {
            nearExit = true;
            addObject = true;
        }

        if (addObject) {
            objectNearPlayer = other.gameObject;
        }
    }

    IEnumerator WeaponFire () { //fires weapon, if effect takes place, it is called and assigned
        while (firing) {
            GameObject bullet = Instantiate (bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
            if (attachments.Count > 0) {
                // Debug.Log ("Firing Weapon");
                for (int i = 0; i < attachments.Count; i++) {
                    if (attachments[i].hasEffect) {
                        if (!attachments[i].onBullet) {
                            AttachmentsEffects.instance.InvokeEffect (attachments[i].functionName, gameObject);
                        } else {
                            bullet.GetComponent<BulletController> ().AssignEffects (attachments[i].onBullet, attachments[i].onBulletEffectHasDelay, attachments[i].onBulletEffectOnceOff, attachments[i].onBulletEffectDelay, attachments[i].functionName);
                        }
                    }
                }

            }
            canFire = false;
            yield return new WaitForSeconds (fireRate);
            canFire = true;
        }
    }

    void FixedUpdate () {
        float moveHorizontal = Input.GetAxisRaw ("Horizontal");
        float moveVertical = Input.GetAxisRaw ("Vertical");
        rb.velocity = new Vector2 (moveHorizontal, moveVertical) * speed;
        Vector3 mouse = Input.mousePosition;
        Vector3 screenPoint = camera.WorldToScreenPoint (transform.localPosition);
        Vector2 offset = new Vector2 (mouse.x - screenPoint.x, mouse.y - screenPoint.y);
        float angle = Mathf.Atan2 (offset.y, offset.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler (0f, 0f, angle + 90f);

    }
}