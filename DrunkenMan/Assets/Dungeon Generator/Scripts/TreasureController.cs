﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureController : MonoBehaviour {
    [SerializeField] GameObject treasureDrop;
    //[SerializeField] Transform dropPoint;
    public void Open () {
        AttachmentsEffects.instance.assignLoot(this.gameObject); //sends Request for loot 
    }

    public void DropLoot (Attachment attachment) { //drops and assigns the loot
        GameObject Dropped = Instantiate (treasureDrop, this.transform.position, this.transform.rotation);
        Dropped.GetComponent<treasure> ().image = attachment.image;
        Dropped.GetComponent<treasure> ().thisAttachment = attachment;
        Destroy (this.gameObject);
    }
}