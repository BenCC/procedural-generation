﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawnerController : MonoBehaviour
{

    [SerializeField] GameObject BossPrefab;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate (BossPrefab, this.transform);
    }


}
