﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class AttachmentsEffects : MonoBehaviour {

    public List<Attachment> attachmentsImplemented;
    public static AttachmentsEffects instance;
    GameObject returnObject;

    private void Start () {
        instance = this;
    }

    public void InvokeEffect (string effect, GameObject _returnObject) {
        Debug.Log ("Invoking"); //Called from bullets or player to call correct Method
        returnObject = _returnObject;
        Invoke (effect, 0);
    }

    void ChangeColor () { //Example of effect, Changes bullet colour
        Debug.Log ("changing Color");
        returnObject.GetComponent<SpriteRenderer> ().color = Color.green;
    }

    public void assignLoot (GameObject chestRequestingLoot) { //assigns loot to chests
        List<Attachment> drops = new List<Attachment> ();
        int lvl = DunGen.instance.currentLevel; 
        for (int i = 0; i < attachmentsImplemented.Count; i++) { //searches through all available drops to see what it can drop, then adds it to a list
            if (attachmentsImplemented[i].levelThreshold <= lvl) {
                if (attachmentsImplemented[i].stopsDropping) {
                    if (attachmentsImplemented[i].lastLevelCanSpawn >= lvl) {
                        drops.Add (attachmentsImplemented[i]);
                    }
                } else {
                    drops.Add (attachmentsImplemented[i]);
                }
            }
        }
        if (drops.Count != 0) {
            StartCoroutine (RollForLoot (drops, chestRequestingLoot));
        }
    }

    IEnumerator RollForLoot (List<Attachment> _list, GameObject _chestReturn) {
        bool lootfound = false;
        int roll; //runs through loot list and rolls until it picks a piece to drop
        int loot = -1;
        while (!lootfound) {
            roll = Random.Range (1, 101);
            for (int b = 0; b < _list.Count; b++) {
                if (roll <= _list[b].spawnPercentage) {
                    loot = b;
                    lootfound = true;
                    break;
                }
            }
            yield return null;
        }
        ReturnLoot (_list[loot], _chestReturn);
    }

    void ReturnLoot (Attachment selectedLoot, GameObject chestReturn) {
        chestReturn.GetComponent<TreasureController> ().DropLoot (selectedLoot); //sends drop back to requesting Chest
    }

}