﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    [SerializeField] GameObject playerTarget;
    [SerializeField] float speed = 5f;
    [SerializeField] float health = 100;

    // Start is called before the first frame update
    void Start () {
        playerTarget = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update () {
        Vector3.MoveTowards(transform.position, playerTarget.transform.position, speed);
        
        if(health <= 0){
            Destroy(gameObject);
        }
    }
}