﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DunGen : MonoBehaviour {

    public static DunGen instance;
    Material genMat;

    [Header ("Generation Settings")]
    [SerializeField] int steps = 500;
    [SerializeField] Vector2Int textureSize;
    [SerializeField] Vector2Int maxRoomSize;
    [SerializeField] Vector2Int minRoomSize;
    [SerializeField] Vector2Int terrainSize;
    [SerializeField] MeshRenderer genRen;
    [SerializeField] int treasureNullRadius;
    [SerializeField] int enemySpawnNullRadius;

    [Header ("Prefabs")]
    [SerializeField] Transform spawnParent;
    [SerializeField] GameObject floorPrefab;
    [SerializeField] Sprite[] floorSprites;
    [SerializeField] GameObject player;
    [SerializeField] GameObject physicalWall;
    [SerializeField] GameObject enemySpawnerPrefab;
    [SerializeField] GameObject bossSpawnerPrefab;
    [SerializeField] GameObject treasurePrefab;
    [SerializeField] GameObject endPointPrefab;
    [SerializeField] GameObject endPointPrefabBoss;
    [SerializeField] GameObject levelPrefab;
    [SerializeField] GameObject EntrancePrefab;

    [Header ("Levels")]
    public int currentLevel;
    [SerializeField] int listLevelValue = 0;
    [SerializeField] bool finiteLevels;
    [SerializeField] List<GameObject> levels;
    [SerializeField] List<Vector3> levelStartPoints;
    [SerializeField] int levelsToGenAhead;
    [SerializeField] int removalThreshold;
    [SerializeField] int StartinglevelsToGen;
    [SerializeField] bool hasBossLevel;
    [SerializeField] int levelsBeforeBoss;
    [SerializeField] int levelsBeforeBossRemaining;

    private void Start () {
        instance = this; // Assign Instance, and set variables needed
        levelsBeforeBossRemaining = levelsBeforeBoss;
        levels = new List<GameObject> ();
        player = GameObject.FindWithTag ("Player");
        genMat = genRen.sharedMaterial;
        currentLevel = 1;
        Generate (true, StartinglevelsToGen); // starts generation of maps
    }

    public void Generate (bool initial, int levelsToGen) {

        bool startLevelsGenerating = true;
        bool bossRoom = false;

        for (int b = 0; b < levelsToGen; b++) {
            if (hasBossLevel) {
                levelsBeforeBossRemaining--; // Decreases count when generating levels to generate boss level
            }

            if (levelsBeforeBossRemaining <= 0 && hasBossLevel) {
                bossRoom = true; //checks if there is a boss room set to spawn, and tells generator to spawn it
                levelsBeforeBossRemaining = levelsBeforeBoss; //resets count
            }
            if (b == levelsToGen - 1 && initial) {
                startLevelsGenerating = false; //used later to start game and place player
            }
            GameObject levelSpawnParent = Instantiate (levelPrefab, spawnParent); //Spawns Empty object used as container
            levelSpawnParent.name = "Level"; // Changes object name
            if (bossRoom) {
                levelSpawnParent.name = "BOSS LEVEL";
            }
            Texture2D tex = new Texture2D (textureSize.x, textureSize.y); // Creates new 2D texture, and set start and end point, as well as middle point of texture
            Vector2Int point = new Vector2Int (textureSize.x / 2, textureSize.y / 2);
            Vector2Int start = new Vector2Int (0, 0);
            Vector2Int end = new Vector2Int (0, 0);
            int directionChangeChance = 0;
            int roomCreateChance = 0; // chances used to generate dungeons

            for (int w = 0; w < textureSize.x; w++) {
                for (int h = 0; h < textureSize.y; h++) {
                    tex.SetPixel (w, h, Color.black); // turns entire texture black
                }
            }

            for (int i = 0; i < steps; i++) { // loops through steps and creates pixel map

                if (i == 1) {
                    start = point; //creates start point
                }

                if (i == steps - 1) {
                    end = point; //creates end point
                }

                int directionRoll; //Roll for creating rooms and changing Directions
                int roomCreateRoll;
                if (bossRoom) {
                    roomCreateRoll = Random.Range (0, 101);
                    directionRoll = 999;
                } else {
                    roomCreateRoll = Random.Range (40, 101);
                    directionRoll = Random.Range (40, 101);
                }

                if (roomCreateRoll <= roomCreateChance) {
                    Vector2Int roomSize = new Vector2Int (Random.Range (minRoomSize.x, maxRoomSize.x), Random.Range (minRoomSize.y, maxRoomSize.y));
                    int xSearchStart = point.x - roomSize.x / 2;
                    int ySearchStart = point.y - roomSize.y / 2;
                    int xSearchEnd = point.x + roomSize.x / 2;
                    int ySearchEnd = point.y + roomSize.y / 2; //if room created, creates a square to create the room from
                    for (int xs = xSearchStart; xs < xSearchEnd; xs++) {
                        for (int ys = ySearchStart; ys < ySearchEnd; ys++) {
                            tex.SetPixel (xs, ys, Color.white); //turn all room pixels white
                        }
                    }
                    roomCreateChance = 0;
                } else {
                    if (!bossRoom) {
                        roomCreateChance += 2;
                    }
                }
                if (directionRoll <= directionChangeChance) { //handles movement of the agent along the path
                    bool sideStep = Random.Range (0, 2) == 0 ? false : true;
                    if (sideStep) {
                        //x movement
                        if (point.x == textureSize.x - 5) {
                            point.x += -1;
                        } else if (point.x == 5) {
                            point.x += 1;
                        } else {
                            point.x += Random.Range (0, 2) == 0 ? -1 : 1;
                        }
                    } else {
                        //y movement
                        if (point.y == textureSize.y - 5) {
                            point.y += -1;
                        } else if (point.y == 5) {
                            point.y += 1;
                        } else {
                            point.y += Random.Range (0, 2) == 0 ? -1 : 1;
                        }
                    }
                    directionChangeChance = 0;
                } else {
                    directionChangeChance += 5;
                }
                tex.SetPixel (point.x, point.y, Color.white);
            }

            tex.Apply (); // applies texture
            AddElements (tex, start, end, levelSpawnParent, startLevelsGenerating, bossRoom); //calls next function
            if (bossRoom) {
                bossRoom = false;
            }
        }
    }

    void AddElements (Texture2D tex, Vector2Int start, Vector2Int end, GameObject levelSpawnParent, bool spawnlevelsGen, bool bossRoom) {
        int treasureChance = 6;
        int enemySpawnerChance = 11;

        for (int x = 0; x < tex.width; x++) {
            for (int y = 0; y < tex.height; y++) {
                if (tex.GetPixel (x, y).ToString () == Color.white.ToString ()) { //Creates a border around the white pixel to create a wall
                    int xSearchStart = x - 1;
                    int ySearchStart = y - 1;
                    int xSearchEnd = x + 2;
                    int ySearchEnd = y + 2;
                    for (int xs = xSearchStart; xs < xSearchEnd; xs++) {
                        for (int ys = ySearchStart; ys < ySearchEnd; ys++) {
                            if (tex.GetPixel (xs, ys).ToString () == Color.black.ToString ()) {
                                tex.SetPixel (xs, ys, Color.green);
                            }
                        }
                    }

                }

            }
        }

        tex.SetPixel (start.x, start.y, Color.cyan);
        tex.SetPixel (end.x, end.y, Color.blue); //sets start and end pixel colours
        bool bossSpawnerPlaced = false;
        for (int x = 0; x < tex.width; x++) {
            for (int y = 0; y < tex.height; y++) {
                if (tex.GetPixel (x, y).ToString () == Color.white.ToString ()) {
                    int SpawnRoll = Random.Range (1, 101);

                    if (SpawnRoll == treasureChance) { //Spawns Treasure, while ensuring not near player
                        bool conflict = false;
                        int xSearchStart = x - treasureNullRadius;
                        int ySearchStart = y - treasureNullRadius;
                        int xSearchEnd = x + treasureNullRadius + 1;
                        int ySearchEnd = y + treasureNullRadius + 1;
                        for (int xs = xSearchStart; xs < xSearchEnd; xs++) {
                            for (int ys = ySearchStart; ys < ySearchEnd; ys++) {
                                if (tex.GetPixel (xs, ys).ToString () == Color.yellow.ToString () || tex.GetPixel (xs, ys).ToString () == Color.cyan.ToString ()) {
                                    conflict = true;
                                    break;
                                }
                            }
                        }
                        if (!conflict) {
                            tex.SetPixel (x, y, Color.yellow);
                        }
                    }
                    if (SpawnRoll == enemySpawnerChance || !bossSpawnerPlaced) {//Spawns Enemy Spawner, while ensuring not near player
                        if (!bossRoom) {
                            bool conflict = false;
                            int xSearchStart = x - enemySpawnNullRadius;
                            int ySearchStart = y - enemySpawnNullRadius;
                            int xSearchEnd = x + enemySpawnNullRadius + 1;
                            int ySearchEnd = y + enemySpawnNullRadius + 1;
                            for (int xs = xSearchStart; xs < xSearchEnd; xs++) {
                                for (int ys = ySearchStart; ys < ySearchEnd; ys++) {
                                    if (tex.GetPixel (xs, ys).ToString () == Color.red.ToString () || tex.GetPixel (xs, ys).ToString () == Color.cyan.ToString ()) {
                                        conflict = true;
                                        break;
                                    }
                                }
                            }
                            if (!conflict) {
                                tex.SetPixel (x, y, Color.red);
                            }
                        } else if(!bossSpawnerPlaced){
                            tex.SetPixel (x, y, Color.red);
                            bossSpawnerPlaced = true;
                        }
                    }

                }
            }
        }

        tex.Apply ();
        genMat.SetTexture ("_BaseMap", tex);
        PlaceSprites (tex, levelSpawnParent, spawnlevelsGen, bossRoom);
    }

    void PlaceSprites (Texture2D tex, GameObject _levelSpawnParent, bool spawnLevelsGen, bool bossRoom) {
        Transform levelSpawnParent = _levelSpawnParent.transform;

        for (int row = 0; row < terrainSize.y; row++) { // Loops through pixel map and uses color information to place prefabs
            for (int col = 0; col < terrainSize.x; col++) {

                int xCoord = Mathf.FloorToInt (((float) col * (float) textureSize.x) / (float) terrainSize.x);
                int yCoord = Mathf.FloorToInt (((float) row * (float) textureSize.y) / (float) terrainSize.y);

                if (tex.GetPixel (xCoord, yCoord).ToString () == Color.white.ToString ()) { // Places Floor
                    int spriteChoice = Random.Range (0, floorSprites.Length); //Picks random floor tiles
                    GameObject newObject = Instantiate (floorPrefab, levelSpawnParent);
                    newObject.GetComponent<SpriteRenderer> ().sprite = floorSprites[spriteChoice];
                    newObject.transform.localPosition = new Vector2 (col, row);
                } else if (tex.GetPixel (xCoord, yCoord).ToString () == Color.cyan.ToString ()) { // Places Player starting position in list to call later
                    GameObject newObject = Instantiate (EntrancePrefab, levelSpawnParent);
                    newObject.transform.localPosition = new Vector2 (col, row);
                    levelStartPoints.Add (newObject.transform.position);
                } else if (tex.GetPixel (xCoord, yCoord).ToString () == Color.green.ToString ()) { //Spawns walls around map
                    GameObject newObject = Instantiate (physicalWall, levelSpawnParent);
                    newObject.transform.localPosition = new Vector2 (col, row);
                } else if (tex.GetPixel (xCoord, yCoord).ToString () == Color.red.ToString ()) {
                    if (!bossRoom) { //Places enemy spawner and adjusts it to current level
                        GameObject newObject = Instantiate (enemySpawnerPrefab, levelSpawnParent);
                        newObject.transform.localPosition = new Vector2 (col, row);
                        int enemySpawns = 2 * currentLevel; 
                        if (enemySpawns > 8) {
                            enemySpawns = 8;
                        }
                        newObject.GetComponent<EnemySpawner> ().enemiesPerSpawn = enemySpawns;
                        float spawnTime = 10 / currentLevel;
                        if (spawnTime < 1) {
                            spawnTime = 1;
                        }
                        newObject.GetComponent<EnemySpawner> ().timeBetweenSpawns = spawnTime;
                    } else { //Places boss Spawner
                        GameObject newObject = Instantiate (bossSpawnerPrefab, levelSpawnParent);
                        newObject.transform.localPosition = new Vector2 (col, row);
                    }
                } else if (tex.GetPixel (xCoord, yCoord).ToString () == Color.yellow.ToString ()) { //Spawns Treasure Chest
                    int spriteChoice = Random.Range (0, floorSprites.Length);
                    GameObject floorObject = Instantiate (floorPrefab, levelSpawnParent);
                    floorObject.GetComponent<SpriteRenderer> ().sprite = floorSprites[spriteChoice];
                    floorObject.transform.localPosition = new Vector2 (col, row);
                    GameObject newObject = Instantiate (treasurePrefab, levelSpawnParent);
                    newObject.transform.localPosition = new Vector2 (col, row);
                } else if (tex.GetPixel (xCoord, yCoord).ToString () == Color.blue.ToString ()) { //Spawns exit point
                    if (!bossRoom) {
                        GameObject newObject = Instantiate (endPointPrefab, levelSpawnParent);
                        newObject.transform.localPosition = new Vector2 (col, row);
                    } else {
                        GameObject newObject = Instantiate (endPointPrefabBoss, levelSpawnParent);
                        newObject.transform.localPosition = new Vector2 (col, row);
                    }
                }

            }
        }
        levels.Add (_levelSpawnParent); //adds level to list and hides it
        _levelSpawnParent.SetActive (false);
        if (!spawnLevelsGen) {
            levels[0].SetActive (true); //sets first level active
            player.transform.position = levelStartPoints[0];
        }
    }

    public void NextLevel () { //called at exits of levels
        if (removalThreshold == listLevelValue + 1) { // checks to see if it should remove any levels
            for (int z = 0; z < removalThreshold; z++) {
                Destroy (levels[0].gameObject); //removes and deletes first element in list
                levels.RemoveAt (0);
            }
            listLevelValue = 0;
            levels[listLevelValue].SetActive (true); //Set first new level as active and moves player
            player.transform.position = levelStartPoints[listLevelValue];
        } else {
            levels[listLevelValue].SetActive (false); //disables old level, moves to next level and moves player
            listLevelValue++;
            levels[listLevelValue].SetActive (true);
            player.transform.position = levelStartPoints[listLevelValue];
        }
        currentLevel++;
        if (!finiteLevels) { // Spawns new levels if told to do so
            Generate (false, levelsToGenAhead);
        }
    }
}